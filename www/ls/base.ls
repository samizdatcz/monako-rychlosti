rychlosti = d3.tsv.parse ig.data.rychlosti, (row, i) ->
  [minutes, seconds] = row.bestlap_cas.split ":"
  minutes = parseInt minutes, 10
  seconds = parseFloat seconds.replace "," "."
  row.time = minutes * 60 + seconds
  row.year = parseInt do
    row.vc_datum.split "." .pop!
    10
  row.speed = parseFloat row.bestlap_rychlost.replace ',' '.'
  row

yearCount = 65
allOkruhy = {}
for lap in rychlosti
  if allOkruhy[lap.vc_trat] is void
    years = [0 to yearCount].map ->
      year: 1950 + it
      x: it
      bestLap: null
    allOkruhy[lap.vc_trat] = {name: lap.vc_trat, years}
  x = lap.year - 1950
  allOkruhy[lap.vc_trat].years[x].bestLap = lap

okruhy = []
for item in window.location.hash.slice 1 .split ","
  okruhy.push allOkruhy[item] if allOkruhy[item]
if !okruhy.length
  okruhy = [allOkruhy["Monza"], allOkruhy["Monaco"]]
dataPoints = []
for okruh in okruhy
  okruh.yearGroups = []
  currentYearGroup = []
  for year in okruh.years
    if year.bestLap is null
      if currentYearGroup.length
        okruh.yearGroups.push currentYearGroup
        currentYearGroup = []
      continue
    dataPoints.push year
    currentYearGroup.push year
  if currentYearGroup.length
    okruh.yearGroups.push currentYearGroup
  okruh.yearGroups.filter -> it.length > 1

container = d3.select ig.containers.base
fullWidth = 1000
fullHeight = 400

margin = top: 140 right: 10px bottom: 35px left: 70px

width = fullWidth - margin.left - margin.right
height = fullHeight - margin.top - margin.bottom

xScale = d3.scale.linear!
  ..domain [0 yearCount]
  ..range [0 width]
yExtent = d3.extent dataPoints.map (.bestLap.speed)

yScale = d3.scale.linear!
  ..domain yExtent
  ..range [height, 0]
line = d3.svg.line!
  ..x -> xScale it.x
  ..y -> yScale it.bestLap.speed

yearMultipier = 1
svg = container.append \svg
  ..attr {width:fullWidth, height:fullHeight}
  ..append \g
    ..attr \class \drawing
    ..attr \transform "translate(#{margin.left},#{margin.top})"
    ..append \g
      ..attr \class \lines
      ..selectAll \g.okruh .data okruhy .enter!append \g
        ..attr \class \okruh
        ..selectAll \path .data (.yearGroups) .enter!append \path
          ..attr \d line
    ..append \g
      ..attr \class \points
      ..selectAll \circle .data dataPoints .enter!append \circle
        ..attr \cx -> xScale it.x
        ..attr \cy -> yScale it.bestLap.speed
        ..attr \r 3
    ..append \g
      ..attr \class \axis
      ..attr \transform "translate(0, #{height + 11})"
      ..selectAll \g.tick .data [0 to yearCount] .enter!append \g
        ..attr \class \tick
        ..attr \transform -> "translate(#{xScale it},0)"
        ..append \line
          ..attr \y2 -> if 0 == it % 10 then 6 else 3
        ..filter (-> yearMultipier > 1 || 0 == it % 10)
          ..append \text
            ..text -> (it * yearMultipier) + 1950
            ..attr \text-anchor ->
              | it == 0 => \start
              | (it * yearMultipier) + 1950 == 2015 => \end
              | otherwise => \middle
            ..attr \y 17
    ..append \g
      ..attr \class \axis
      ..attr \transform "translate(-5, 0)"
      ..selectAll \g.tick .data yExtent .enter!append \g
        ..attr \class \tick
        ..attr \transform -> "translate(0, #{yScale it})"
        ..append \line
          ..attr \x2 -3
        ..append \text
          ..html -> "#{ig.utils.formatNumber it} km/h"
          ..attr \text-anchor \end
          ..attr \x -9
          ..attr \y 3
      # ..append \line
      #   ..attr \y1 yScale yExtent.0

points = svg.selectAll \circle

container.append \ul
  ..attr \class \legend
  ..selectAll \li .data okruhy .enter!append \li
    ..html -> it.name

highlightPoint = (point) ->
  points.classed \active -> it is point
  x = margin.left + xScale point.x
  y = margin.top - 7 + yScale point.bestLap.speed
  lap = point.bestLap
  text = "<b>#{lap.bestlap_cas} #{lap.vuz_jezdec}</b><em>#{lap.vc_datum}, #{lap.jezdec_team}</em><span class='supplemental'>Průměrná rychlost #{ig.utils.formatNumber lap.speed, 1} km/h<br>Vůz #{lap.vuz_sasi_znacka} #{lap.vuz_sasi_nazev}, motor #{lap.vuz_motor_znacka} #{lap.vuz_motor_nazev}</span>"
  graphTip.display x, y, text

downlightPoint = ->
  points.classed \active no
  graphTip.hide!

polygon = ->
  "M#{it.join "L"}Z"

voronoi = d3.geom.voronoi!
    ..x ~> margin.left + xScale it.x
    ..y ~> margin.top + yScale it.bestLap.speed
    ..clipExtent [[0, 0], [fullWidth, fullHeight - 24]]
voronoiPolygons = voronoi dataPoints
  .filter -> it

tipArea = container.append \div
  ..attr \class \graph-tips

graphTip = new ig.GraphTip tipArea
voronoiSvg = container.append \svg
  ..attr \class \voronoi
  ..attr {width:fullWidth, height:fullHeight}
  ..selectAll \path .data voronoiPolygons .enter!append \path
    ..attr \d polygon
    ..on \mouseover -> highlightPoint it.point
    ..on \touchstart -> highlightPoint it.point
    ..on \mouseout -> downlightPoint!

